﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousOrbit : MonoBehaviour {

	public Transform target;          // Цель для камеры
	public float distance = 2.0f;     // Дистанция от цели то камеры
	public float min = -20;
	public float max = 60;             
	private float xRot;
	private float yRot;
	public float ms=2.0f;              // Чувствительность мышки

	// Use this for initialization
	void Start () {
		
		Cursor.lockState = CursorLockMode.Locked;                               // Скрываем мышку
		transform.position = target.position - transform.forward * distance;    // Устанавливаем начальное положение камеры
	}
	
	// Update is called once per frame
	void Update () {

		// Показать курсор

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		// Получение Input от мышки

		yRot += Input.GetAxis ("Mouse X") * ms;
		xRot -= Input.GetAxis ("Mouse Y") * ms;
		xRot = Mathf.Clamp (xRot, min, max);

		// Поворот камеры

		transform.rotation = Quaternion.Euler (xRot, yRot, 0.0f);

		// Коллизия камеры ( с рывками, подёргиванием камеры) 

		RaycastHit info;

		if (Physics.Linecast (target.position, transform.position, out info)) {
			if (info.collider.tag == "Wall")
				transform.position = target.position - transform.forward * info.distance;    // Тут не уверен, но работает вроде правильно, только рывками.
		} 
		else transform.position = target.position - transform.forward * distance;

	}
}


