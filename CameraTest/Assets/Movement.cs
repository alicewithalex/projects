﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float speed = 5.0f;
	public float force = 1000.0f;
	private Rigidbody rb;
	private bool isGrounded;

	// Use this for initialization
	void Start () {


		rb = GetComponent<Rigidbody> ();
		if (rb.velocity.y == 0) {
			isGrounded = true;
		} else
			isGrounded = false;

	}
	
	// Update is called once per frame
	void Update () {

		if (rb.velocity.y == 0) {
			isGrounded = true;
		} else
			isGrounded = false;

		if (Input.GetKeyDown (KeyCode.Space) && isGrounded)
			rb.AddForce (Vector3.up * force, ForceMode.Force);


		transform.Translate (Input.GetAxis ("Horizontal") * speed * Time.deltaTime, 0.0f, Input.GetAxis ("Vertical") * speed * Time.deltaTime);
		transform.Rotate (0.0f, Input.GetAxis ("Mouse X") * 2, 0.0f);

	}
}

